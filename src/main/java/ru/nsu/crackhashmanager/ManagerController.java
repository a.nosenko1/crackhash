package ru.nsu.crackhashmanager;

import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.nsu.crackhashmanager.model.CrackHashWorkerResponse;
import ru.nsu.crackhashmanager.model.CrackRequest;
import ru.nsu.crackhashmanager.model.CrackResponse;
import ru.nsu.crackhashmanager.model.StatusResponse;
import ru.nsu.crackhashmanager.service.ManagerBrain;


@RestController()
@RequestMapping()
@AllArgsConstructor
public class ManagerController {
    static final Logger log =
            LoggerFactory.getLogger(ManagerController.class);
    @Autowired
    final ManagerBrain managerBrain;

    @PostMapping(value = "/api/hash/crack")
    public CrackResponse crack(@RequestBody CrackRequest crackRequest) {
        String requestID = managerBrain.createNewCrackTask(crackRequest.getHash(), crackRequest.getMaxLength());
        return new CrackResponse(requestID);
    }

    @GetMapping(value = "/api/hash/status")
    public StatusResponse status(@RequestParam String requestId) {
        return new StatusResponse(managerBrain.getTaskStatus(requestId), managerBrain.getTaskData(requestId));
    }
    @PatchMapping(value = "/internal/api/manager/hash/crack/request")
    public void request(@RequestBody CrackHashWorkerResponse response) {
        log.info("Received response " + response.getRequestId());
        log.info("Received answers " + response.getAnswers().getWords());

        managerBrain.receiveWorkerResponse(response.getRequestId(), response.getAnswers().getWords());
    }
}
