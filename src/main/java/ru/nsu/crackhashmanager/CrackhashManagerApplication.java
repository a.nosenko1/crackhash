package ru.nsu.crackhashmanager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrackhashManagerApplication {
	static final Logger log =
			LoggerFactory.getLogger(CrackhashManagerApplication.class);
	public static void main(String[] args) {
		SpringApplication.run(CrackhashManagerApplication.class, args);
	}

}
