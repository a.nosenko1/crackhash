package ru.nsu.crackhashmanager.service;

import com.fasterxml.jackson.module.jaxb.JaxbAnnotationModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.nsu.crackhashmanager.model.CrackHashManagerRequest;
import ru.nsu.crackhashmanager.model.CrackTask;
import ru.nsu.crackhashmanager.model.ObjectFactory;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class ManagerBrain {

    static final String resourceUrl = "http://localhost:8081/internal/api/worker/hash/crack/task";
    static final Logger log = LoggerFactory.getLogger(ManagerBrain.class);
    public ManagerBrain(){
        char[] symbols = "abcdefghijklmnopqrstuvwxyz1234567890".toCharArray();
        for (char s: symbols) {
            alphabet.getSymbols().add(String.valueOf(s));
        }
    }
    private static final int workers = 1;

    CrackHashManagerRequest.Alphabet alphabet = new CrackHashManagerRequest.Alphabet();
    ConcurrentHashMap<String, CrackTask> tasksMap = new ConcurrentHashMap<String, CrackTask>();

    public String createNewCrackTask(String hash, int maxLength){

        log.info("Creating a new task: hash = " + hash);

        String requestID = java.util.UUID.randomUUID().toString();
        CrackTask crackTask = new CrackTask(requestID, hash, maxLength, CrackTask.TaskStatus.IN_PROGRESS, workers);
        tasksMap.put(requestID, crackTask);

        for (int i = 0; i < workers; i++) {
            postTask(requestID, hash, maxLength, i, workers);
        }

        return requestID;
    }

    public String getTaskStatus(String requestID){
        return tasksMap.get(requestID).getTaskStatus().toString();
    }
    public String[] getTaskData(String requestID){
        String[] data = tasksMap.get(requestID).getData();
        if (data.length > 0)
            return data;
        return null;
    }

    private void postTask(String requestID, String hash, int maxLength, int partNumber, int partCount){
        RestTemplate restTemplate = new RestTemplate();

        try{
            ObjectFactory objectFactory = new ObjectFactory();
            CrackHashManagerRequest request = objectFactory.createCrackHashManagerRequest();

            request.setRequestId(requestID);
            request.setHash(hash);
            request.setAlphabet(alphabet);
            request.setMaxLength(maxLength);
            request.setPartNumber(partNumber);
            request.setPartCount(partCount);

            HttpEntity<CrackHashManagerRequest> request1 = new HttpEntity<>(request);
            log.info("Request with RequestId = " + request1.getBody().getRequestId() + " formed");

            restTemplate.postForObject(resourceUrl, request1, CrackHashManagerRequest.class);
            log.info("Sent post request with RequestId = " + request1.getBody().getRequestId() +
                    "; partNumber=" + partNumber);

        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    public void receiveWorkerResponse(String requestID, List<String> words){
        tasksMap.get(requestID).addWords(words);
    }

}
