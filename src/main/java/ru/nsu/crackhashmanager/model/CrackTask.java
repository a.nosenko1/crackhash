package ru.nsu.crackhashmanager.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RequiredArgsConstructor
public class CrackTask {

    @AllArgsConstructor
    public enum TaskStatus {

        IN_PROGRESS (1, "IN_PROGRESS"),
        READY (2, "READY"),
        ERROR (3, "ERROR");

        private final Integer value;
        private final String label;
    }

    @Getter
    private final String requestID;
    @Getter
    private final String hash;
    @Getter
    private final int maxLength;
    private final Set<String> data = Collections.synchronizedSet(new HashSet<>());
    @NonNull
    private TaskStatus taskStatus;
    @NonNull
    private int workers;

    private void addWord(String word){
        data.add(word);
    }
    public String[] getData(){
        return data.toArray(String[]::new);
    }
    public synchronized void setStatus(TaskStatus newStatus){
        taskStatus = newStatus;
    }
    public synchronized TaskStatus getTaskStatus(){
        return taskStatus;
    }
    public synchronized void addWords(List<String> words){
        workers -= 1;
        for(String w : words){
            addWord(w);
        }
        if (workers == 0){
            setStatus(TaskStatus.READY);
        }
    }
}
